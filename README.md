# Test framework architecture sample for Miro register page

This project is representing test framework sample. Test object is Miro register page: https://miro.com/signup/

## Test design mind map

I made up a mind map with the main ideas what and how to test:
![Test-analysis-mind-map](docs/Miro%20signUp%20page.png)
I have not had a chance to cover all checks from the map, so I covered only a part of functionality.

### In scope of testing:

- Functional testing of registration feature with Name, Email and Password
- Checks across different languages, such as English (latin), Russian (cyrillic) and German (extended latin with
  umlauts)
- Cross-browser testing (Chrome, Firefox, Safari, Edge)

### Out of scope of testing:

- SSO authorization (via G-Suite, Slack, etc)
- Cross-platform testing with different resolutions
- Miro Source dropdown values selection
- Receiving news and updates
- Sign in after the registration

### Discovered issues

- The system does not have frontend validation for spaces in password field, so it causes funny behavior: the system
  admits the password with only spaces but then the error message appear "Please enter the password"

*Steps to reproduce*:

1. Fill in the name, email fields with valid values (like Anna Kh, anna.kh@gmail.com)
2. Fill in the password field with 8 or more spaces
3. Click the checkbox "I agree to Miro Terms and Privacy Policy."

*Actual result*: the password was evaluated as so-so, but then backend validation trimmed the spaces, and the error
message appeared: "Please enter your password."

- The system does not allow registering with special characters, cyrillic and umlauts in the email. It should be allowed
  according to the standard: https://datatracker.ietf.org/doc/html/rfc5322
- The previous error about wrong password does not disappear after I changed the value:
  ![old error message screenshot](docs/old_error_message.png)

## Languages and Frameworks

This project using the following languages and frameworks:

* [Java 8](https://openjdk.java.net/projects/jdk/8/) as the programming language
* [Apache Maven](https://maven.apache.org/) as the package management tool
* [TestNG](https://testng.org/doc/) as the UnitTest framework to support the test creation
* [Selenium WebDriver](https://www.selenium.dev/) as the web browser automation framework using the Java binding
* [AssertJ](https://joel-costigliola.github.io/assertj/) as the fluent assertion library
* [Allure Report](https://docs.qameta.io/allure/) as the testing report strategy
* [JavaFaker](https://github.com/DiUS/java-faker) as the faker data generation strategy
* [Log4J2](https://logging.apache.org/log4j/2.x/) as the logging management strategy
* [WebDriverManager](https://github.com/bonigarcia/webdrivermanager) as the Selenium binaries management
* [Owner](http://owner.aeonbits.org/) to minimize the code to handle the properties file
* [Lombok](https://projectlombok.org/) to minimize the code for getters, setters and other service constructs

## Test framework main components

![Test-analysis-mind-map](docs/project_architecture.png)

## How to run tests

```mvn clean test allure:serve```
This command will run all tests from src/test/resources/suites/local.xml TestNG suite in two browsers by default: Chrome
and Firefox. Then it will open the new page in your default browser with a generated allure report:
![Allure_video](docs/allure_presentation.mov)

### Configuration

If you need to run tests in other browsers, change the following parameter in local.xml:

`        <parameter name="browser" value="firefox"/>`

The framework also supports run from IDE. For local usage it will use browser from `src/test/resources/local.properties`
. Change it for execution on other browsers, the appropriate driver will be downloaded automatically.

You can also switch on/off headless mode in `src/test/resources/configuration.properties`. Pay attention, that Safari
driver does not support headless mode.

If you have any questions or suggestions, please contact me by email: testprogmath@gmail.com 

