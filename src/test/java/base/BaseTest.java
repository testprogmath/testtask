package base;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import ru.annachemic.driver.DriverManager;
import ru.annachemic.driver.TargetFactory;
import ru.annachemic.listeners.TestListener;

import static ru.annachemic.config.ConfigurationManager.configuration;

@Slf4j
@Listeners({TestListener.class})
public abstract class BaseTest {


    @BeforeSuite(alwaysRun = true)
    @Parameters("browser")
    public void preCondition(@Optional("chrome") String browser) {
//        if (browser.equals(null)) browser = configuration().browser();
        WebDriver driver = new TargetFactory().createInstance(browser);
        DriverManager.setDriver(driver);

    }

    @BeforeMethod
    public void openThePage() {
        DriverManager.getDriver().get(configuration().hostname() + configuration().registerPath());
    }

    @AfterSuite(alwaysRun = true)
    public void postCondition() {
        DriverManager.quit();
    }

}
