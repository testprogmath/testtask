package ru.annachemic.test;

import base.BaseTest;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.annachemic.config.AllureManager;
import ru.annachemic.data.RegisterFormDataFactory;
import ru.annachemic.enums.ErrorMessages;
import ru.annachemic.model.RegistrationData;
import ru.annachemic.pages.RegistrationPage;
import ru.annachemic.test.dataprovider.RegistrationTestsDataProvider;

@Feature("Submit registration")
public class RegistrationTests extends BaseTest {
    RegistrationData defaultData;
    RegistrationPage registrationPage;

    @BeforeSuite
    void setAllureEnvironment() {
        AllureManager.setAllureEnvironmentInformation();

    }

    @BeforeTest
    void setUp() {
        registrationPage = new RegistrationPage();
        defaultData = new RegisterFormDataFactory().createRegistrationData();
    }

    @Test(description = "Positive default registration test")
    @Severity(SeverityLevel.BLOCKER)
    void positiveDefaultRegistrationTest() {
        registrationPage
                .closeCookiesDialog()
                .enterName(defaultData.getName())
                .enterEmail(defaultData.getEmail())
                .enterPassword(defaultData.getPassword())
                .selectTermsCheckbox()
                .clickSubmitButton()
                .checkPageElements();
    }

    @Test(description = "Positive parametrized emails registration test", dataProvider = "positiveEmailValues", dataProviderClass = RegistrationTestsDataProvider.class)
    @Severity(SeverityLevel.CRITICAL)
    void positiveParametrizedEmailRegistrationTest(String email, String description) {
        registrationPage
                .closeCookiesDialog()
                .enterName(defaultData.getName())
                .enterEmail(email)
                .enterPassword(defaultData.getPassword())
                .selectTermsCheckbox()
                .clickSubmitButton()
                .checkPageElements();
    }

    @Test(description = "Positive parametrized passwords registration test", dataProvider = "positivePasswordValues", dataProviderClass = RegistrationTestsDataProvider.class)
    @Severity(SeverityLevel.CRITICAL)
    void positiveParametrizedPasswordsRegistrationTest(String password, String passwordHint, String description) {
        //to avoid "this email has been already registered" error message
        defaultData = new RegisterFormDataFactory().createRegistrationData();
        registrationPage
                .closeCookiesDialog()
                .enterName(defaultData.getName())
                .enterEmail(defaultData.getEmail())
                .enterPassword(password)
                .checkPasswordHint(passwordHint)
                .selectTermsCheckbox()
                .clickSubmitButton()
                .checkPageElements();
    }

    @Test(description = "Positive name parametrized test", dataProvider = "positiveNameValues", dataProviderClass = RegistrationTestsDataProvider.class)
    @Severity(SeverityLevel.CRITICAL)
    void positiveNameRegistrationTest(String name, String description) {
        defaultData = new RegisterFormDataFactory().createRegistrationData();
        registrationPage
                .closeCookiesDialog()
                .enterName(name)
                .enterEmail(defaultData.getEmail())
                .enterPassword(defaultData.getPassword())
                .selectTermsCheckbox()
                .clickSubmitButton()
                .checkPageElements();
    }

    @Test(description = "Check that registration is not possible without terms acceptance")
    @Severity(SeverityLevel.NORMAL)
    void registrationIsImpossibleWithoutTermsTest() {
        registrationPage
                .closeCookiesDialog()
                .enterName(defaultData.getName())
                .enterEmail(defaultData.getEmail())
                .enterPassword(defaultData.getPassword())
                .clickSubmitButtonAndVerify()
                .checkErrorMessage(ErrorMessages.NO_TERMS);
    }

    @Test(description = "Negative parametrized emails registration test", dataProvider = "negativeEmailValues", dataProviderClass = RegistrationTestsDataProvider.class)
    @Severity(SeverityLevel.MINOR)
    void negativeParametrizedEmailRegistrationTest(String email, String description) {
        registrationPage
                .closeCookiesDialog()
                .enterName(defaultData.getName())
                .enterEmail(email)
                .enterPassword(defaultData.getPassword())
                .selectTermsCheckbox()
                .clickSubmitButtonAndVerify()
                .checkErrorMessage(ErrorMessages.NOT_AN_EMAIL);
    }

    @Test(description = "Negative test with already registered email")
    @Severity(SeverityLevel.NORMAL)
    void negativeTestWithAlreadyRegisteredEmail() {
        registrationPage
                .closeCookiesDialog()
                .registration(defaultData.getName(), defaultData.getEmail(), defaultData.getPassword());
        openThePage();
        registrationPage.enterName(defaultData.getName())
                .enterEmail(defaultData.getEmail())
                .enterPassword(defaultData.getPassword())
                .selectTermsCheckbox()
                .clickSubmitButtonAndVerify()
                .checkErrorMessage(ErrorMessages.EMAIL_IS_REGISTERED);
    }

    @Test(description = "Negative password length test", dataProvider = "negativePasswordValues", dataProviderClass = RegistrationTestsDataProvider.class)
    @Severity(SeverityLevel.NORMAL)
    void negativePasswordsRegistrationTest(String password, ErrorMessages messages, String description) {
        //to avoid "this email has been already registered" error message
        defaultData = new RegisterFormDataFactory().createRegistrationData();
        registrationPage
                .closeCookiesDialog()
                .enterName(defaultData.getName())
                .enterEmail(defaultData.getEmail())
                .enterPassword(password)
                .selectTermsCheckbox()
                .clickSubmitButtonAndVerify()
                .checkErrorMessage(messages);
    }


}


