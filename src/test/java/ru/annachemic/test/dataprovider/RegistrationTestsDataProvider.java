package ru.annachemic.test.dataprovider;

import com.github.javafaker.Faker;
import org.testng.annotations.DataProvider;
import ru.annachemic.enums.ErrorMessages;
import ru.annachemic.helpers.DataHelper;

public class RegistrationTestsDataProvider {
    static Faker faker = new Faker();

    @DataProvider
    public static Object[][] positiveEmailValues() {
        return new Object[][]{
                {faker.name().username() + "@bing.com", "latin name, lower case"},
                {DataHelper.generateCyrillicEmail(), "cyrillic email"},
                {DataHelper.generateCyrillicString(10) + "@gmail.com", "cyrillic domain"},
                {faker.animal().name() + "üöä" + "." + faker.name().lastName() + "@gmail.com", "german umlauts"},
                {faker.animal().name() + "." + faker.name().lastName() + "@rambler.com", "dot inside name, mixed case"},
                {faker.name().firstName() + "-" + faker.name().lastName() + "@mail.ru", "hyphen inside the name, top-level domain contains 2 letters"},
                {faker.name().firstName() + "_" + faker.name().lastName() + "@gmail.com", "underscore inside the name, 3 letters in top-level domain"},
                {faker.name().firstName().toUpperCase() + "_" + faker.name().lastName().toUpperCase() + "@mail.com".toUpperCase(), "underscore inside the name, upper case"},
                {faker.animal().name() + "." + faker.name().lastName() + "@yandex.ru", "top-level domain contains 2 letters"},
                {DataHelper.generateNumericString(8) + "@inbox.ru", "numbers in the user part"},
                {faker.name().lastName() + DataHelper.generateNumericString(8) + "@cambridge.co.uk", "string + numbers in the user part, multipart domain"},
                {faker.name().firstName() + "+" + DataHelper.generateNumericString(5) + "@yandex.ru", "+ sign and numbers in the user part"},
                {DataHelper.generateLatinString(64) + "@yandex.ru", "max length of the user part"},
                {DataHelper.generateLatinString(1) + "@" + faker.name().username() + ".com", "min length of the user part"},
                {faker.name().firstName() + "@" + DataHelper.generateLatinString(253) + ".com", "max length of the domain part"},
                {faker.name().username() + "@" + DataHelper.generateNumericString(1) + ".com", "min length of the domain part"},
        };
    }

    @DataProvider
    public static Object[][] positivePasswordValues() {
        return new Object[][]{
                {"test tes", "So-so password", "lower case, latin symbols, space inside, 8 symbols (minimal length)"},
                {"grünerWald", "Good password", "mixed case, umlauts"},
                {"MALTA?IS#THE.BEST!56", "Great password", "upper case, special characters, numbers"},
                {DataHelper.generateLatinString(60), "Great password", "max length is 60 symbols"}
        };
    }

    @DataProvider
    public static Object[][] negativeEmailValues() {
        return new Object[][]{
                {faker.name().username() + "gmail.ru", "without @"},
                {faker.name().username() + "@", "without whole domain part"},
                {faker.name().username() + "@gmail", "without top-level domain part"},
                {"@gmail.com", "without name"},
                {"vasilisa%^&@gmail.com", " with special symbols in the user part"},
                {"vasilisa.alb@gm%$*ail.com", " with special symbols in the domain part"},
                {faker.name().username() + faker.name().firstName() + "@gmail.o", "1 letter in top-level domain"},

        };
    }

    @DataProvider
    public static Object[][] negativePasswordValues() {
        return new Object[][]{
//                {DataHelper.generateLatinString(7), ErrorMessages.SMALL_PASSWORD, "less than 8 symbols"},
                {"", ErrorMessages.EMPTY_PASSWORD, "empty password"},
                {"        ", ErrorMessages.SPACES_IN_PASSWORD, "only spaces"},
                {DataHelper.generateNumericString(61), ErrorMessages.TOO_LONG_PASSWORD, "more than 60 symbols"}

        };
    }

    @DataProvider
    public static Object[][] positiveNameValues() {
        return new Object[][]{
                {faker.name().fullName(), "latin symbols, mixed case, complex name with spaces within"},
                {faker.name().firstName().toUpperCase(), "simple latin name in upper case"},
                {"анна-мария", "cyrillic symbols, complex name with -, lower case"},
                {faker.internet().emailAddress(), "email"},
                {"O'Connor Sara", "complex latin name with '"},
                {"Herbert Grün-de-Wald", "name with umlaut"},
                {"奥古斯丁娜", "chinese characters"},
                {"A", "1 letter - the smallest name"},
                {DataHelper.generateLatinString(55), "the largest allowed name"}
        };
    }
}
