package ru.annachemic.pages;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.annachemic.base.BasePage;
import ru.annachemic.helpers.AssertHelper;


public class CheckEmailPage extends BasePage {


    @FindBy(css = "h1.signup__title-form")
    private WebElement pageHeader;
    @FindBy(css = ".signup__subtitle-form")
    private WebElement textParagraph;
    @FindBy(css ="#code")
    private WebElement codeInput;
    @FindBy(css ="a[href*='get-new-code']")
    private WebElement sendCodeAgainLink;
    @FindBy(css = "a[href*='Registration-Confirmation-Issues']")
    private WebElement helpCenterLink;



    public CheckEmailPage enterCode(String code) {
        codeInput.sendKeys(code);
        return this;
    }
    public CheckEmailPage requestToSendCodeAgain() {
        sendCodeAgainLink.click();
        return this;
    }

    public CheckEmailPage checkPageElements() {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(AssertHelper.isElementDisplayed(pageHeader)).isTrue();
        softly.assertThat(AssertHelper.isElementDisplayed(textParagraph)).isTrue();
        softly.assertThat(AssertHelper.isElementDisplayed(codeInput)).isTrue();
        softly.assertThat(AssertHelper.isElementDisplayed(sendCodeAgainLink)).isTrue();
        softly.assertThat(AssertHelper.isElementDisplayed(helpCenterLink)).isTrue();
        softly.assertAll();
        return this;
    }
}
