package ru.annachemic.pages;

import io.qameta.allure.Step;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.annachemic.base.BasePage;
import ru.annachemic.driver.DriverManager;
import ru.annachemic.enums.ErrorMessages;
import ru.annachemic.helpers.AssertHelper;

import static org.assertj.core.api.Assertions.assertThat;

public class RegistrationPage extends BasePage {


    @FindBy(name = "signup[name]")
    private WebElement loginInput;
    @FindBy(name = "signup[email]")
    private WebElement workEmail;
    @FindBy(name = "signup[password]")
    private WebElement passwordInput;
    @FindBy(className = "signup__input-hint-text")
    private WebElement passwordHintLabel;
    @FindBy(className = "speero-dropdown__title-wrap")
    private WebElement source;
    @FindBy(css = "label[for='signup-terms']")
    private WebElement termsCheckbox;
    @FindBy(name = "signup[subscribe]")
    private WebElement newsCheckbox;
    @FindBy(css = "button[data-autotest-id='mr-form-signup-btn-start-1']")
    private WebElement submitButton;

    @FindBy(id = "onetrust-accept-btn-handler")
    private WebElement cookieAcceptButton;


    @Step("I enter the name {name}")
    public RegistrationPage enterName(String name) {
        loginInput.sendKeys(name);
        return this;
    }

    @Step("I enter the working email {email}")
    public RegistrationPage enterEmail(String email) {
        workEmail.sendKeys(email);
        return this;
    }

    @Step("I enter the working password {password}")
    public RegistrationPage enterPassword(String password) {
        passwordInput.sendKeys(password);
        return this;
    }

    @Step("I select signUp terms checkbox")
    public RegistrationPage selectTermsCheckbox() {
        termsCheckbox.click();
        return this;
    }

    @Step("I select news checkbox")
    public RegistrationPage selectNewsCheckbox() {
        newsCheckbox.click();
        return this;
    }

    @Step("I click the submit button")
    public CheckEmailPage clickSubmitButton() {
        submitButton.click();
        return new CheckEmailPage();
    }

    @Step("I click the submit button")
    public RegistrationPage clickSubmitButtonAndVerify() {
        submitButton.click();
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(AssertHelper.isElementDisplayed(source)).isTrue();
        softly.assertThat(AssertHelper.isElementDisplayed(submitButton)).isTrue();
        softly.assertThat(AssertHelper.isElementDisplayed(loginInput)).isTrue();
        softly.assertThat(AssertHelper.isElementDisplayed(passwordInput)).isTrue();
        softly.assertThat(AssertHelper.isElementDisplayed(workEmail)).isTrue();
        softly.assertAll();
        return this;
    }

    @Step("Registration")
    public CheckEmailPage registration(String name, String email, String userPassword) {
        enterName(name);
        enterEmail(email);
        enterPassword(userPassword);
        selectTermsCheckbox();
        clickSubmitButton();
        return new CheckEmailPage();
    }


    public RegistrationPage closeCookiesDialog() {
            try {
                cookieAcceptButton.click();
            } catch (Exception e) {
                logger.info("The element not present");
            }
        return this;
    }

    public RegistrationPage checkPasswordHint(String passwordHint) {
        assertThat(passwordHintLabel.getText()).isEqualTo(passwordHint);
        return this;
    }

    public RegistrationPage checkErrorMessage(ErrorMessages errorMessage) {
        WebElement element = DriverManager.getDriver().findElement(errorMessage.getCssLocator());
        assertThat(element.getText()).contains(errorMessage.getMessage());
        return this;
    }

}
