package ru.annachemic.base;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import ru.annachemic.driver.DriverManager;

import static ru.annachemic.config.ConfigurationManager.configuration;

public abstract class BasePage {
    protected static final Logger logger = LogManager.getLogger(BasePage.class);

    protected BasePage() {
        PageFactory.initElements(new AjaxElementLocatorFactory(DriverManager.getDriver(), configuration().timeout()), this);
    }

}
