package ru.annachemic.enums;

import java.util.Random;

public enum MiroInfoSource {
    SEARCH_ENGINE("Search Engine (Google, Bing)"),
    UNKNOWN("Not Sure / Don’t Remember"),
    WORK_SCHOOL("Through work or school"),
    TV("TV"),
    RADIO("Radio / Streaming Audio"),
    SOCIAL_MEDIA("Social Media"),
    PODCAST("Podcast"),
    OTHER("Other"),
    YOUTUBE("Online Video (Youtube)"),
    MAIL("In the mail"),
    FRIEND("Friend / Colleague"),
    BLOG("Blog / Article");

    private final String value;

    MiroInfoSource(String value) {
        this.value = value;
    }

    public static MiroInfoSource getRandom() {
        return values()[new Random().nextInt(values().length)];
    }

    @Override
    public String toString() {
        return value;
    }
}
