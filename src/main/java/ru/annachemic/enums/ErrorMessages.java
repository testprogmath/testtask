package ru.annachemic.enums;

import lombok.Getter;
import org.openqa.selenium.By;

public enum ErrorMessages {
    TOO_LONG_PASSWORD("Sorry, your password cannot exceed 60 characters", "[data-autotest-id=mr-error-passwordtoolong-1]"),
    SMALL_PASSWORD("Please use 8+ characters for secure password", ".signup__input-hint-text"),
    EMPTY_PASSWORD("Please enter your password.", "[data-autotest-id=please-enter-your-password-1]"),
    SPACES_IN_PASSWORD("Please enter your password.", "[data-autotest-id='mr-error-signup.error.emptypassword-1']"),
    NO_TERMS("Please agree with the Terms to sign up.", "[data-autotest-id='mr-error-signup.error.emptyterms-1']"),
    NOT_AN_EMAIL("This doesn’t look like an email address. Please check it for typos and try again.", "[data-autotest-id=mr-error-emailformat-1]"),
    EMAIL_IS_REGISTERED("Sorry, this email is already registered", "[data-autotest-id=mr-error-emailnotunique-1]");

    @Getter
    private String message;
    @Getter
    private By cssLocator;

    ErrorMessages(String message, String locator) {
        this.message = message;
        this.cssLocator =  By.cssSelector(locator);
    }
}
