package ru.annachemic.helpers;

import lombok.experimental.UtilityClass;

import java.util.Arrays;
import java.util.Locale;
import java.util.Random;

@UtilityClass
public class DataHelper {
    Random r = new Random();
    private static final String RUS = "абвгдеёжзийклмнопрстуфхцчъыьэюя";
    private static final String ENG = "abcdefghijklmnopqrstuvwxyz";
    private static final String DIGITS = "0123456789";

    public String generateCyrillicString(int len) {
        return generateString(len, RUS);
    }

    public String generateNumericString(int len) {
        return generateString(len, DIGITS);
    }

    public String generateLatinString(int len) {
        return generateString(len, ENG);
    }

    public String generateCyrillicEmail() {
        return generateCyrillicString(10) + "@" + generateCyrillicString(6) + ".рф";
    }

    public String generateString(int len, String locale) {
        String sum = locale + locale.toUpperCase();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < len; i++) {
            builder.append(sum.charAt(r.nextInt(sum.length())));
        }
        return builder.toString();
    }
}
