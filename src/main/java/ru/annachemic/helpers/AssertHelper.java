package ru.annachemic.helpers;

import lombok.experimental.UtilityClass;
import org.openqa.selenium.WebElement;

import java.util.NoSuchElementException;

@UtilityClass
public class AssertHelper {
    public boolean isElementDisplayed(WebElement element) {
        try {
            return element.isDisplayed();
        } catch(NoSuchElementException e) {
            return false;
        }
    }

}
