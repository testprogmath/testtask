package ru.annachemic.data;

import com.github.javafaker.Faker;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.annachemic.enums.MiroInfoSource;
import ru.annachemic.model.RegistrationData;


public class RegisterFormDataFactory {


    private final Faker faker;
    private static final Logger logger = LogManager.getLogger(RegisterFormDataFactory.class);

    public RegisterFormDataFactory() {
        faker = new Faker();
    }

    public RegistrationData createRegistrationData() {
        RegistrationData registrationData = RegistrationData.builder().
                name(faker.name().fullName()).
                email(faker.name().firstName() + faker.internet().emailAddress()).
                password(faker.internet().password()).
                miroInfoSource(MiroInfoSource.getRandom()).
                build();

        logger.info(registrationData);
        return registrationData;
    }

}
