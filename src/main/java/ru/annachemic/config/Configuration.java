package ru.annachemic.config;

import org.aeonbits.owner.Config;

@Config.Sources({
        "file:src/test/resources/configuration.properties",
        "classpath:local.properties"
})
public interface Configuration extends Config {
    String hostname();

    @DefaultValue("/signup")
    String registerPath();

    boolean headless();

    @DefaultValue("local")
    String target();

    @Key("timeout")
    int timeout();

    @Key("local.browser")
    @DefaultValue("chrome")
    String browser();
}
