package ru.annachemic.config;

import com.github.automatedowl.tools.AllureEnvironmentWriter;
import com.google.common.collect.ImmutableMap;
import io.qameta.allure.Attachment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.TakesScreenshot;
import ru.annachemic.driver.DriverManager;

import static org.openqa.selenium.OutputType.BYTES;
import static ru.annachemic.config.ConfigurationManager.configuration;

public class AllureManager {
    private static final Logger logger = LogManager.getLogger(AllureManager.class);
    private AllureManager() {
    }

    public static void setAllureEnvironmentInformation() {
        AllureEnvironmentWriter.allureEnvironmentWriter(
                ImmutableMap.<String, String>builder().
                        put("Test URL", configuration().hostname()).
                        put("Target execution", configuration().target()).
                        put("Global timeout", String.valueOf(configuration().timeout())).
                        put("Headless mode", String.valueOf(configuration().headless())).
                        put("Local browser", configuration().browser()).
                        build());
    }

    @Attachment(value = "Failed test screenshot", type = "image/png")
    public static byte[] takeScreenshotToAttachOnAllureReport() {
        return ((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(BYTES);
    }

    @Attachment(value = "Browser information", type = "text/plain")
    public static String addBrowserInformationOnAllureReport() {
        return DriverManager.getInfo();
    }

}
