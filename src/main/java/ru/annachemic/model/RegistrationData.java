package ru.annachemic.model;

import lombok.*;
import ru.annachemic.enums.MiroInfoSource;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderClassName = "RegistrationBuilder")
public class RegistrationData {
    private String email;
    private String name;
    private String password;
    private MiroInfoSource miroInfoSource;

}
